# -*- coding: utf-8 -*-
"""
Created on Sun Oct 07 14:44:47 2018

@author: guoyu
"""

import numpy as np
from HG_cavity import HG_cavity
from spin_dynamics import MultipleSpins
import matplotlib.pyplot as plt

#setting up the spin geometry. 
#random positions bounded by a box in the first quadrant
def random_positions(sites,xlim,ylim):
    
    positions = np.zeros((sites,2))
    for i in range(sites):
        positions[i] = np.array([xlim*np.random.random(),ylim*np.random.random()])
            
    return positions

#setup the cavity
#the first two variables set the waist in microns
#the last two variables set the offset from the origin
hg = HG_cavity(35,35,0,0)

random_spins = MultipleSpins(50,random_positions(50,100.0,100.0),10.0e3,3.0,150.0,80.0,1000.0,hg,30)

random_spins.set_equation(eqn_type='ramp')

initial_state = random_spins.initialize_spins()

random_spins.time_evolve(initial_state,0,1e-3,5)
amplitudes = random_spins.light_field()
random_spins.animate_light_field(coarse=10)
#random_spins.plot_evolution()