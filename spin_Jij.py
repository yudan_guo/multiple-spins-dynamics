# -*- coding: utf-8 -*-
"""
Spin dynamics for a Jij matrix
"""

import numpy as np
from scipy.integrate import ode

class SpinJij:
    def __init__(self,Nspins,Jij,omega,omega_0,kappa,pump_rabi):
        '''
        Nspins: number of spins
        positions: position of spins
        omega_0: transverse field 
        omega: cavity detuning to first mode 
        kappa: cavity loss rate
        epsilon: linear mode dispersion
        pump_rabi: effective rabi frequency of the pump beam, g Omega/Delta_a
        all energy is measured in kHz
        '''
        
        self.Nspins = Nspins
        self.Jij = Jij
        self.omega = omega
        self.omega_0 = omega_0
        self.kappa = kappa
        self.pump_rabi = pump_rabi
        
        self.Du = (-8*omega/(omega**2+kappa**2))*Jij
        self.Dg = (-16*omega_0*omega*kappa/(omega**2+kappa**2)**2)*Jij
        
        
    def set_equation(self,eqn_type = 'ramp'):
        
        def spin_eom(t,spin_vector):
            '''
            Spin vector embedding
            sx_i first N entries
            sy_i next N entries
            sz_i last N entries
            '''            
            Ns = self.Nspins
            pumping = (self.pump_rabi)**2
            
            sx_i = spin_vector[:Ns]
            sy_i = spin_vector[Ns:(2*Ns)]
            sz_i = spin_vector[(2*Ns):]
        
            sxdot_i = -self.omega_0*sy_i
            sydot_i = self.omega_0*sx_i - pumping*sz_i*np.dot(self.Du,sx_i) - pumping*sz_i*np.dot(self.Dg,sy_i)
            szdot_i = pumping*sy_i*np.dot(self.Du,sx_i) + pumping*sy_i*np.dot(self.Dg,sy_i)
            
            return np.append([sxdot_i,sydot_i],szdot_i)
        
        def spin_eom_ramp(t,spin_vector):
            '''
            Spin vector embedding
            sx_i first N entries
            sy_i next N entries
            sz_i last N entries
            '''            
            Ns = self.Nspins
            pumping = (self.pump_rabi)**2
            
            sx_i = spin_vector[:Ns]
            sy_i = spin_vector[Ns:(2*Ns)]
            sz_i = spin_vector[(2*Ns):]
        
            sxdot_i = -self.omega_0*sy_i
            sydot_i = self.omega_0*sx_i - t*pumping*sz_i*np.dot(self.Du,sx_i) - t*pumping*sz_i*np.dot(self.Dg,sy_i)
            szdot_i = t*pumping*sy_i*np.dot(self.Du,sx_i) + t*pumping*sy_i*np.dot(self.Dg,sy_i)
            
            return np.append([sxdot_i,sydot_i],szdot_i)
        
        if eqn_type == 'ramp':
            self.integrator = ode(spin_eom_ramp)
        if eqn_type == 'constant':
            self.integrator = ode(spin_eom)
        self.integrator.set_integrator("vode")
        
    def time_evolve(self,initial_variables,initial_time,time_step,final_time):
        self.integrator.set_initial_value(initial_variables,initial_time)
        
        #array to store time and evolution results
        self.time_evolution = np.array(np.append([initial_time],initial_variables))
        while self.integrator.successful() and self.integrator.t <= final_time:
            t = self.integrator.t + time_step
            y = self.integrator.integrate(t)
            self.time_evolution = np.vstack((self.time_evolution,np.append([t],y)))
            
        print 'Integration Done'
                        
    def initialize_spins(self):
        '''
        Helper function for initializing the initial spin states
        small random polarization along x
        '''
        initial_state = np.zeros(3*self.Nspins)
        
        for i in range(self.Nspins):
            rnd_num = np.random.random()
            if rnd_num <0.5:
                initial_spin = np.array([-0.001,0,-0.5])
            else:
                initial_spin = np.array([0.001,0,-0.5])
                
            initial_state[i] = initial_spin[0]
            initial_state[self.Nspins+i] = initial_spin[1]
            initial_state[2*self.Nspins+i] = initial_spin[2]
            
        return initial_state