# -*- coding: utf-8 -*-
"""

"""

import numpy as np
import scipy
import cmath
from scipy.interpolate import interp1d

class HG_cavity:
    
    def __init__(self,WX,WY,X0,Y0):
        self.WX = WX
        self.WY = WY
        self.X0 = X0
        self.Y0 = Y0
        
        #Load the look up table for fast evaluation of Hermite polynomials
        HGtable = scipy.load('HGhash_0_100.npy')
        self.HGtable = HGtable;
        self.x0 = HGtable[-1,0];
        self.dx = HGtable[-1,1]-HGtable[-1,0];       
        
        #Generate the interpolation function
        self.HGinterp = [];
        for m in range(0,101):
            self.HGinterp.append(interp1d(HGtable[-1],HGtable[m,:],assume_sorted=1))
            
    def HG1D(self,x,p=[0,0,1]):
        """
        1D HG for use with levlab.fit.Fit
            x = point to evaluate
            
            p = HG parameters
                p[0] = order of HG
                p[1] = center position
                p[2] = waist
        """
        
                          
        
        " Using scipy.interpolate with lookup table "
#        return self.HGinterp[int(p[0])]((x-p[1])/p[2])
        
        " Conventional lookup method. Assumes x is at most a 1D array. "
        xprime = (x-p[1])/p[2];
        ind = scipy.rint((xprime-self.x0)/self.dx)
        return self.HGtable[int(p[0]),ind.astype(int)]
    
    def HG2D(self,X,P=[0,0,1,0,0,1]):
        """
        2D HG 
            X = point to evaluate
                X[0] = x-coordinate
                X[1] = y-coordinate
                
            P = HG parameters
                P[0] = x-order of HG
                P[1] = x-center position
                P[2] = x-waist
                P[3] = y-order of HG
                P[4] = y-center position
                P[5] = y-waist
        """


        
        " Assumes a square grid and uses seperability of 2D HGs "
        x = X[0][0,:]; y = X[1][:,0]
        HGx = self.HG1D(x,[P[0],P[1],P[2]]);
        HGy = self.HG1D(y,[P[3],P[4],P[5]]);
        HGmesh = scipy.meshgrid(HGx,HGy);
        return HGmesh[0]*HGmesh[1]

    def HG2D_lm(self,x,y,l,m):
        '''
        2D HG for use to calculate interaction matrix
        return the value of the lm mode at position x,y
        '''
        HGx_val = self.HG1D(x,[l,self.X0,self.WX])
        HGy_val = self.HG1D(y,[m,self.Y0,self.WY])
        
        return HGx_val*HGy_val

    def family_int(self,positions,order):
        '''
        Given a list of positions, calculate the interaction matrix
        for a family of HG. 
        '''
        Ns = len(positions)
        Jij = np.zeros((Ns,Ns))
        for l in range(order+1):
            m = order-l
            #evaluate the mode function at all positions
            HG_vals = self.HG2D_lm(positions[:,0],positions[:,1],l,m)
            
            #add the interaction from the mode to the Jij matrix
            Jij = Jij + np.outer(HG_vals,HG_vals)
            
        return Jij

    def family_Dmatrices(self,positions,order,omega,omega_0,kappa,epsilon):
        '''
        Directly calculated the Dmatrices for the spin dynamics equation
        assuming a small astigmatic splitting epsilon
        '''
        Ns = len(positions)
        Du = np.zeros((Ns,Ns))
        Dg = np.zeros((Ns,Ns))
        for l in range(order+1):
            m = order-l
            #evaluate the mode function at all positions
            HG_vals = self.HG2D_lm(positions[:,0],positions[:,1],l,m)
            
            #add the contribution from each modes
            gij = np.outer(HG_vals,HG_vals)
            Du = Du - 8*gij*(omega+l*epsilon)/((omega+l*epsilon)**2+kappa**2)
            Dg = Dg - 16*omega_0*gij*omega_0*kappa*(omega+l*epsilon)/((omega+l*epsilon)**2+kappa**2)**2
            
        return Du,Dg
    
    def family_singleoverlap(self,positions,order,epsilon,kappa,weights):
        '''
        evaluates the overlap between the mode and the spins
        i.e. the value of the mode at the spin
        epsilon kappa here are dimensionless: epsilon/Delta_c, kappa/Delta_c
        '''
        Ns = len(positions)
        amplitude = np.zeros(order+1,dtype = complex)
        for l in range(order+1):
            m = order-l
            #evaluate the mode function at all positions
            HG_val = 0
            for i in range(Ns):
                HG_val = HG_val + weights[i]*self.HG2D_lm(positions[i,0],positions[i,1],l,m)    
            amplitude[l] = HG_val/(1+l*epsilon+1j*kappa)
            
        return amplitude
    
    def family_field(self,X,P):
        """
        Evaluates the field of a superposition of HGs from a given l+m
        family. The values of the center position are allowed to vary.
            
            X = point to evaluate
                X[0] = x-coordinate
                X[1] = y-coordinate
            
            P = Parameters
                P[0] = family number (l+m)
                Amplitudes:
                    P[1] = amplitude of HG_N0
                    P[2] = amplitude of HG_(N-1)1 etc...
                    P[N+1] = amplitude of HG_0N
                Phases:
                    P[N+1+1] = phase of HG_N0
                    P[N+1+2] = phase of HG_(N-1)1 etc...
                    P[2*(N+1)] = phase of HG_0N
                Centers:
                    P[2*(N+1)+1] = x0
                    P[2*(N+1)+2] = y0
        """
        # get HG family number
        N = int(P[0]);
        
        # pull out positions and waists        
        X0 = self.X0; Y0 = self.Y0;
        WX = self.WX; WY = self.WY;
        field = scipy.zeros(X[0].shape,dtype=scipy.complex128);
        
        # calculate field of the superposition
        for n in range(0,N+1):
            field += cmath.rect(P[n+1],P[(N+1)+(n+1)])*self.HG2D(X,[N-n,X0,WX,n,Y0,WY]);
    
        # return field
        return field