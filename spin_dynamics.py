import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from scipy.integrate import ode

class MultipleSpins:
    def __init__(self,Nspins,positions,omega,omega_0,kappa,epsilon,pump_rabi,hg,order):
        '''
        Nspins: number of spins
        positions: position of spins
        omega_0: transverse field 
        omega: cavity detuning to first mode 
        kappa: cavity loss rate
        epsilon: linear mode dispersion
        hg: an instance of HG_cavity to calculate D matrices
        the default hg is initialized to 35 um waist
        order: order of the transverse mode family to be used
        pump_rabi: effective rabi frequency of the pump beam, g Omega/Delta_a
        all energy is measured in kHz
        '''
        
        self.Nspins = Nspins
        self.positions = positions
        self.omega = omega
        self.omega_0 = omega_0
        self.kappa = kappa
        self.pump_rabi = pump_rabi
        self.hg = hg
        self.order = order
        self.epsilon = epsilon
        
        self.Du,self.Dg = hg.family_Dmatrices(positions,order,omega,omega_0,kappa,epsilon)
        
        
    def set_equation(self,eqn_type = 'ramp'):
        
        def spin_eom(t,spin_vector):
            '''
            Spin vector embedding
            sx_i first N entries
            sy_i next N entries
            sz_i last N entries
            '''            
            Ns = self.Nspins
            pumping = (self.pump_rabi)**2
            
            sx_i = spin_vector[:Ns]
            sy_i = spin_vector[Ns:(2*Ns)]
            sz_i = spin_vector[(2*Ns):]
        
            sxdot_i = -self.omega_0*sy_i
            sydot_i = self.omega_0*sx_i - pumping*sz_i*np.dot(self.Du,sx_i) - pumping*sz_i*np.dot(self.Dg,sy_i)
            szdot_i = pumping*sy_i*np.dot(self.Du,sx_i) + pumping*sy_i*np.dot(self.Dg,sy_i)
            
            return np.append([sxdot_i,sydot_i],szdot_i)
        
        def spin_eom_ramp(t,spin_vector):
            '''
            Spin vector embedding
            sx_i first N entries
            sy_i next N entries
            sz_i last N entries
            '''            
            Ns = self.Nspins
            pumping = (self.pump_rabi)**2
            
            sx_i = spin_vector[:Ns]
            sy_i = spin_vector[Ns:(2*Ns)]
            sz_i = spin_vector[(2*Ns):]
        
            sxdot_i = -self.omega_0*sy_i
            sydot_i = self.omega_0*sx_i - t*pumping*sz_i*np.dot(self.Du,sx_i) - t*pumping*sz_i*np.dot(self.Dg,sy_i)
            szdot_i = t*pumping*sy_i*np.dot(self.Du,sx_i) + t*pumping*sy_i*np.dot(self.Dg,sy_i)
            
            return np.append([sxdot_i,sydot_i],szdot_i)
        
        if eqn_type == 'ramp':
            self.integrator = ode(spin_eom_ramp)
        if eqn_type == 'constant':
            self.integrator = ode(spin_eom)
        self.integrator.set_integrator("vode")
        
    def time_evolve(self,initial_variables,initial_time,time_step,final_time):
        self.integrator.set_initial_value(initial_variables,initial_time)
        
        #array to store time and evolution results
        self.time_evolution = np.array(np.append([initial_time],initial_variables))
        while self.integrator.successful() and self.integrator.t <= final_time:
            t = self.integrator.t + time_step
            y = self.integrator.integrate(t)
            self.time_evolution = np.vstack((self.time_evolution,np.append([t],y)))
            
        print 'Integration Done'
        
    def light_field(self):
        '''
        find the time evolution of the light field 
        from the spin time evolution
        '''
        #extract the sx alignment
        sx = self.time_evolution[:,1:(self.Nspins+1)]
        
        amplitudes = np.zeros((len(sx),self.order+1),dtype=complex)
        
        #calculate the field
        #the amplitude is the value of the HG mode at the position of the spin
        for i in range(len(sx)):
            epsilon_t = self.epsilon/self.omega_0
            kappa_t = self.kappa/self.omega_0
            amplitudes[i] = self.hg.family_singleoverlap(self.positions,self.order,epsilon_t,kappa_t,sx[i,:])
        self.amplitudes = amplitudes
        print 'Light field calculated'
        
        return amplitudes
    
    def animate_light_field(self,coarse = 1):
        plotting_amp = np.abs(self.amplitudes[::int(coarse),:])
        plotting_phase = np.angle(self.amplitudes[::int(coarse),:])
        x,y = np.meshgrid(np.linspace(-200,200,401),np.linspace(-200,200,401))

        fig = plt.figure()
        ims = []
        for i in range(len(plotting_amp)):
            P = np.concatenate(([self.order],plotting_amp[i],plotting_phase[i],[0,0]))
            im = plt.imshow(np.abs(self.hg.family_field((x,y),P)),clim = (0,5e-4),cmap = 'inferno',animated = True)
            ims.append([im])
            
        self.ani = animation.ArtistAnimation(fig, ims, interval=50, blit=True)
        
        plt.show()
                        
    def initialize_spins(self):
        '''
        Helper function for initializing the initial spin states
        small random polarization along x
        '''
        initial_state = np.zeros(3*self.Nspins)
        
        for i in range(self.Nspins):
            rnd_num = np.random.random()
            if rnd_num <0.5:
                initial_spin = np.array([-0.001,0,-0.5])
            else:
                initial_spin = np.array([0.001,0,-0.5])
                
            initial_state[i] = initial_spin[0]
            initial_state[self.Nspins+i] = initial_spin[1]
            initial_state[2*self.Nspins+i] = initial_spin[2]
            
        return initial_state
    
    def plot_evolution(self,axis=0):
        '''
        plot time evolution
        axis = 0 for sx
             = 1 for sy
             = 2 for sz
        '''
        for k in range(self.Nspins):
            plt.plot(self.time_evolution[:,0],self.time_evolution[:,k+1+axis*self.Nspins])