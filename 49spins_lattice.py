# -*- coding: utf-8 -*-
"""
Created on Sat Sep 22 17:14:33 2018

@author: guoyu
"""

import numpy as np
from HG_cavity import HG_cavity
from spin_dynamics import MultipleSpins
import matplotlib.pyplot as plt

#setting up the spin geometry. 
#a square lattice in the first quadrant on the cavity plane
#avoid the cavity mode symmetry
def lattice(sites,period):
    
    positions = np.zeros((sites*sites,2))
    for i in range(sites):
        for j in range(sites):
            positions[i*sites+j] = np.array([i*period,j*period])
            
    return positions

#setup the cavity
#the first two variables set the waist in microns
#the last two variables set the offset from the origin
hg = HG_cavity(35,35,0,0)

square_lattice = MultipleSpins(49,lattice(7,9.4),5.0e3,3.0,150.0,80.0,500.0,hg,30)

square_lattice.set_equation(eqn_type='ramp')

initial_state = square_lattice.initialize_spins()

square_lattice.time_evolve(initial_state,0,1e-4,10)

square_lattice.plot_evolution(axis=0)

#for plotting evolution as a matrix
#each column represents a time trace
#plt.imshow(square_lattice.time_evolution[:,100:149],aspect = 1,interpolation=None,extent=(-0.5, 6.5, -0.5, 5.5),cmap='bwr')
