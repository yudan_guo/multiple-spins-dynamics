# -*- coding: utf-8 -*-
"""
Created on Thu Oct 04 15:08:45 2018

dynamics of a random Jij matrix.
using the spin_Jij class.
"""

import numpy as np
from spin_Jij import SpinJij
import matplotlib.pyplot as plt

Ns = 50

#create a random symmetric matrix, with off diagonal entries between -1 and 1
rnd_matrix = np.random.random((50,50))-0.5
rnd_Jij = rnd_matrix + np.transpose(rnd_matrix)
np.fill_diagonal(rnd_Jij,3.14)

randomJij = SpinJij(Ns,rnd_Jij,5e3,3.0,132,400)
randomJij.set_equation('constant')
initial_state = randomJij.initialize_spins()

randomJij.time_evolve(initial_state,0,5e-4,5)

for k in range(randomJij.Nspins):
    plt.plot(randomJij.time_evolution[:,0],randomJij.time_evolution[:,k+1+0*randomJij.Nspins])
    
