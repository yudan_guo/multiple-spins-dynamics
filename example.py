# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 11:16:08 2018

@author: guoyu
"""

import numpy as np
from HG_cavity import HG_cavity
from spin_dynamics import MultipleSpins
import matplotlib.pyplot as plt

#setting up the spin geometry. In this case an equilateral triangle with each corner
#distance r from the origin
def triangle(r):
    return np.array([[0,r],[0.5*np.sqrt(3)*r,-0.5*r],[-0.5*np.sqrt(3)*r,-0.5*r]])

#setup the cavity
#the first two variables set the waist in microns
#the last two variables set the offset from the origin
hg = HG_cavity(35,35,0,0)


#initialize the multiple spin class
'''
time is measured in milliseconds
frequency is measured in kHz

arguments
MultipleSpins(Nspins,positions,omega,omega_0,kappa,epsilon,pump_rabi,hg,order)
'''
threespin = MultipleSpins(3,triangle(24.5),5e3,3,150,0,400,hg,1)
threespin.set_equation(eqn_type='ramp')

#set initial spins
initial_spin = np.array([0.001,0,-1])
initial_spin = 0.5*initial_spin/np.linalg.norm(initial_spin)
initial_state = np.zeros(9)
initial_state[:3] = initial_spin[0]
initial_state[3:6] = initial_spin[1]
initial_state[6:] = initial_spin[2]

#evolve
'''
time_evolve(initial_state,start_time,time_step,end_time)
'''
threespin.time_evolve(initial_state,0,5e-5,3)

plt.plot(threespin.time_evolution[:,0],threespin.time_evolution[:,1])
plt.plot(threespin.time_evolution[:,0],threespin.time_evolution[:,2])
plt.plot(threespin.time_evolution[:,0],threespin.time_evolution[:,3])
