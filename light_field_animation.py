# -*- coding: utf-8 -*-
"""
Created on Mon Oct 01 10:39:58 2018

@author: guoyu
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from HG_cavity import HG_cavity

plotting_amp = np.abs(amplitudes)
plotting_phase = np.angle(amplitudes)

hg = HG_cavity(35,35,0,0)
start_P = np.concatenate(([30],plotting_amp[0],plotting_phase[0],[0,0]))

x,y = np.meshgrid(np.linspace(-200,200,401),np.linspace(-200,200,401))

fig = plt.figure()
ims = []
for i in range(len(plotting_amp)):
    P = np.concatenate(([30],plotting_amp[i],plotting_phase[i],[0,0]))
    im = plt.imshow(np.abs(hg.family_field((x,y),P)),clim = (0,1e-3),cmap = 'inferno',animated = True)
    ims.append([im])
    
ani = animation.ArtistAnimation(fig, ims, interval=50, blit=True)

plt.show()